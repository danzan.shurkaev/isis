#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <fcntl.h>

#define GET 0
#define POST 1
struct sockaddr_in ad;

int Connect()
{
    int sc;
	sc = socket(AF_INET, SOCK_STREAM, 0);
	if (sc < 0) { 
		printf("response receiving error\n");
        return -1;
	}	
	struct hostent * ht;
	
	ht = gethostbyname("www.iu3.bmstu.ru");
	if (ht == NULL) {
		printf("Hostname wasn't received\n"); 
        return -1;
	}

	/* Подготовим структуру заполнив ее нулями */
	memset(&ad, 0, sizeof(ad));
    ad.sin_family = AF_INET;

	/* Копируем первый адрес из структуры hostent */
	memcpy(&ad.sin_addr.s_addr, ht->h_addr_list[0], ht->h_length);

	/* Номер порта сервера */
	ad.sin_port = htons(8090);	

    if (connect(sc, (struct sockaddr *)&ad,sizeof(ad)) < 0) {
        printf("Can't connect to server\n"); 
        return -1;
    }

	return sc;
}

int Request(const int connect, char* buf, const int size, const int req, char* type, char* format)
{
    int len;
    if (req == GET)
    {
        len = sprintf(buf, "GET /WebApi/time?type=%s&format=%s HTTP/1.0\n\n", type, format);
    }
    else if (req == POST)
    {
        len = sprintf(buf, "POST /WebApi/time HTTP/1.0\nContent-Type:application/x-www-form-urlencoded\nContent-Length:20\n\ntype=%s&format=%s\n", type, format);
    }

    if (send(connect, buf, len, 0) < 0)
    {
        printf("request sending error\n");
        len = -1;
    }

    len = 0;
    int count = 1;
    while (count > 0){
        count = recv(connect, buf + len, size, 0);
        len += count;
    }    
    buf[len] = '\0';
    return len;
}

int GetHTTPCode(char * buf)
{
    const int codePos = 9;
    char code[4];
    code[3] = '\0';
    memcpy(code, buf + codePos, 3);
    return atoi(code);
}

char * GetHTTPContent(char * buf)
{    
    for(int i =0; buf[i] != '\0'; i++)
        return buf + i;   
}

int main(int argc, char** argv)
{
    char * type = "utc"; // local or utc
    char * format = "internet"; // internet or unix
    int req = GET;   
    
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-t") == 0)
        {
            type = argv[++i];
        }
        else if (strcmp(argv[i], "-f") == 0)
        {
            format = argv[++i];
        }
        else if (strcmp(argv[i], "-POST") == 0)
        {
            req = POST;
        }
        else if (strcmp(argv[i], "-GET") == 0)
        {
            req = GET;
        }
        else
        {
            printf("key not recognized\n");
            return 1;
        }
    }

    const int connect = Connect();
    if (connect == -1)
        return 1;    

    const int bufSize = 260;
    char buf[bufSize];
    if (Request(connect, buf, bufSize, req, type, format) == -1)
    {
        return 1;
    }

    const int code = GetHTTPCode(buf);
    if (code != 200)
    {
        printf("HTTP error code %d\n", code);
        return 1;
    }
    else
    {
        char * res = GetHTTPContent(buf);
        printf("%s\n", res);
    }
    
    return 0;
}
