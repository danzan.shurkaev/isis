/*
 *  Задание #6
 *  Автор: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "map.h"

void undecorate_name(char * name)
{
    int cnt;
    while (name[cnt]) {
        if (name[cnt] == '_')
            name[cnt] = ' ';
        cnt++;
    }
}

int main(int argc, char * argv[])
{
    COUNTRY ** map = map_load();

    int err = 0;
    char cmd[10];
    char name[50];
    int population;
    int area;

    while (1)
    {
        printf(">>");
        
        scanf("%s", cmd);

        if (strcmp(cmd, "add") == 0)
        {            
            scanf("%s", name);
            undecorate_name(name);            
            scanf("%d", &population);            
            scanf("%d", &area);

            map_add(map, name, population, area);
        }
        else if (strcmp(cmd, "delete") == 0)
        {
            scanf("%s", name);
            undecorate_name(name);

            map_delete(map, name);
        }
        else if (strcmp(cmd, "dump") == 0)
        {
           map_dump(map);
        }
        else if (strcmp(cmd, "view") == 0)
        {
            scanf("%s", name);
            undecorate_name(name);

            COUNTRY* p = map_find(map, name);
            if (!p)
            {
                printf("country doesn't exists\n");
                err = 1;
            }
            else
            {
                print_country(p);
            }
        }
        else if (strcmp(cmd, "count") == 0)
        {
            printf("%d\n", map_count(map));
        }
        else if (strcmp(cmd, "save") == 0)
        {
            map_save(map);
        }
        else if (strcmp(cmd, "quit") == 0)
        {
            break;
        }
        else
        {
            printf("command not found\n");
        }
    }
    
    map_save(map);
    map_clear(map);
    
    return err;
}
