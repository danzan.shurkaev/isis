#include <stdio.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdlib.h>
#define file_name "/dev/hello"


typedef struct led {
    int pin;
    unsigned char value;
} led_t;

#define HELLO_MAJOR 200

#define LEDCMD_RESET_STATE _IO(HELLO_MAJOR, 1)                 /* погасить все индикаторы */
#define LEDCMD_GET_STATE _IOR(HELLO_MAJOR, 2, unsigned char *) /* вернуть состояния всех индикаторов */
#define LEDCMD_GET_LED_STATE _IOWR(HELLO_MAJOR, 3, struct led_t *) /* вернуть значение индикатора с указанным номером */
#define LEDCMD_SET_LED_STATE _IOW(HELLO_MAJOR, 4, struct led_t *) /* установить значение индикатора с указанным номером */ 

int f;

int set_led_state(int pin, char val) {
    int fd = open("/dev/hello", O_RDWR);
    led_t led = {
		.pin 	= pin, 
		.value 	= val
	};
    int err = ioctl(fd, LEDCMD_SET_LED_STATE, &led); // Получение текущего состояния
    if (err) {
        printf("error on set\n");
        close(fd);
        return err;
    }
    printf("pin: %d, val: %d\n", led.pin, led.value);
    close(fd);
    return 0;
}

int get_led_state(int pin) {
    led_t led = { pin = pin };
    int fd = open("/dev/hello", O_RDWR);
    if (fd == -1) {
        return -1;
    }
    int err = ioctl(fd, LEDCMD_GET_LED_STATE, &led);	// Получение текущего состояния
    if (err) {
        printf("error on get\n");
        close(fd);
        return err;
    }
    printf("pin: %d, val: %d\n", led.pin, led.value);
    close(fd);
    return 0;
}

int reset_leds() {
    int fd 	= open("/dev/hello", O_RDWR);
    int err = ioctl(fd, LEDCMD_RESET_STATE);
    if (err) {
        printf("error on get\n");
        close(fd);
        return err;
    }
    close(fd);
    return 0;
}

int get_all_leds() {
    unsigned char state;
    int err = ioctl(f, LEDCMD_GET_STATE, &state);
    if (err != 0)
    {
        printf("ledstate: error\n");
    }
    else
    {
        for (int i = 0; i < 8; ++i)
        {
            unsigned char value = (state & (128 >> i)) != 0;
            printf("%d", value);
        }
        printf("\n");
    }
    return err;
}


int main(int argc, char** argv) {

    f = open(file_name, O_RDWR); 
    if (f == -1)
    {
        printf("device opening error\n");
        return -1;
    }

    int err = 0;

    if (argc == 2) {
        if (!strcmp(argv[1], "reset")) {
            err = reset_leds();
        } else if (!strcmp(argv[1], "ledstate")) {
            err = get_all_leds();
        }
    } else if (argc == 3) {
        if (!strcmp(argv[1], "on")) {
            char* end;
            int pin = (int) strtol(argv[2], &end, 10);
            err = set_led_state(pin, 1);
        } else if (!strcmp(argv[1], "off")) {
            char* end;
            int pin = (int) strtol(argv[2], &end, 10);
            err = set_led_state(pin, 0);
        } else if (!strcmp(argv[1], "ledstate")) {
            char* end;
            int pin = (int) strtol(argv[2], &end, 10);
            err = get_led_state(pin);
        }
    }
    else
    {
        err = -1;
    }
    
    return err;
}